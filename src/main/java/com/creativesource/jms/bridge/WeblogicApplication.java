package com.creativesource.jms.bridge;

import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.stream.javadsl.SourceQueueWithComplete;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.time.Duration;
import java.util.Hashtable;

@QuarkusMain
public class WeblogicApplication {
    private static final String JNDI_CFACTORY="weblogic.jndi.WLInitialContextFactory";
    //private static final String JNDI_JMS_CFACTORY="/Concessionaria1027ConnectionFactory";
    //private static final String WEBLOGIC_URL = "t3://172.16.132.1:4311,172.16.132.1:4321";
    //private static final String JNDI_JMS_TOPIC="passagensprocessadas.1027";
    //private static final String USERNAME = "c1027";
    //private static final String PASSWORD = "GFn7CEkZ";
    private static final String JNDI_JMS_CFACTORY="/Concessionaria1013ConnectionFactory";
    private static final String WEBLOGIC_URL = "t3://10.255.248.1:1211,10.255.248.1:1212,10.255.248.1:1221,10.255.248.1:1222";
    private static final String JNDI_JMS_TOPIC="passagensprocessadas.1013";
    private static final String USERNAME = "c1013";
    private static final String PASSWORD = "M09$T%5s1i";

    private static final String SUBSCRIBER = "test-subscriber";
    private static final String CLIENT_ID = "test-clientid-passagemprocessada";

    public static void main(String... args) {
        Quarkus.run(Weblogic.class, args);
    }

    public static class Weblogic implements QuarkusApplication, MessageListener {

        private final ActorSystem system;
        private final SourceQueueWithComplete<BytesMessage> queue;

        public Weblogic(){
            system = ActorSystem.create("bridge-system");
            ActorMaterializer materializer = ActorMaterializer.create(system);

            queue = Source.<BytesMessage>queue(10, OverflowStrategy.backpressure())
                    .throttle(10, Duration.ofSeconds(3))
                    .to(Sink.foreach(elem -> {
                        System.out.println(
                                String.format("Mensagem recebida. Id: %s. Timestamp: %s. Destination: %s",
                                        elem.getJMSMessageID(), elem.getJMSTimestamp(), elem.getJMSDestination()));

                        byte[] bytes = new byte[(int) elem.getBodyLength()];
                        elem.readBytes(bytes);
                        System.out.println(String.format("Body: %s", bytes));
                    }))
                    .run(materializer);
        }

        @Override
        public int run(String... args) throws Exception {
            int exitCode = 0;
            try{
                System.out.println("Startup....");
                Hashtable env = new Hashtable();
                env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_CFACTORY);
                env.put(Context.PROVIDER_URL, WEBLOGIC_URL);

                System.out.println("Try bind on ConnectionFactory...");
                InitialContext ic=new InitialContext(env);

                TopicConnectionFactory f=(TopicConnectionFactory)ic.lookup(JNDI_JMS_CFACTORY) ;

                System.out.println(
                        String.format("Try connecting with Username %s and Password %s...", USERNAME, PASSWORD));
                TopicConnection con=f.createTopicConnection(USERNAME, PASSWORD);
                con.setClientID(CLIENT_ID);
                con.start();

                System.out.println("Creating Topic Session...");
                TopicSession ses=con.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
                Topic t=(Topic)ic.lookup(JNDI_JMS_TOPIC);

                System.out.println(String.format("Creating durable subscriber %s with client %s", SUBSCRIBER, CLIENT_ID));
                TopicSubscriber subscriber=ses.createDurableSubscriber(t, SUBSCRIBER);

                subscriber.setMessageListener(this);

                Thread.sleep(Integer.MAX_VALUE);
            }catch (Exception e) {
                System.err.println("Error on bid topic " );
                e.printStackTrace();
                exitCode = 1;
            }

            return exitCode;
        }

        @Override
        public void onMessage(Message message) {
            BytesMessage bytesMessage;

            if (message instanceof BytesMessage) {
                bytesMessage = ((BytesMessage)message);
                queue.offer(bytesMessage);
                /*String messageID = bytesMessage.getJMSMessageID();
                long timestamp = bytesMessage.getJMSTimestamp();
                Destination destination = bytesMessage.getJMSDestination();
                System.out.println(
                        String.format("Mensagem recebida. Id: %s. Timestamp: %s. Destination: %s",
                                messageID,timestamp,destination.toString()));

                byte[] bytes = new byte[(int) bytesMessage.getBodyLength()];
                bytesMessage.readBytes(bytes);
                System.out.println(String.format("Body: %s", bytes));*/
            } else {
                System.out.println("Mensagem recebida porem nao processada");
            }
        }
    }
}