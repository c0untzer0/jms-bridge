package com.creativesource.jms.bridge;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import org.apache.activemq.artemis.core.config.impl.ConfigurationImpl;
import org.apache.activemq.artemis.core.server.ActiveMQServer;
import org.apache.activemq.artemis.core.server.ActiveMQServers;
import org.apache.activemq.artemis.jms.bridge.JMSBridge;
import org.apache.activemq.artemis.jms.bridge.QualityOfServiceMode;
import org.apache.activemq.artemis.jms.bridge.impl.JMSBridgeImpl;
import org.apache.activemq.artemis.jms.bridge.impl.JNDIConnectionFactoryFactory;
import org.apache.activemq.artemis.jms.bridge.impl.JNDIDestinationFactory;

import javax.naming.InitialContext;
import java.util.Hashtable;

//@QuarkusMain
public class BridgeApplication {
    private static final String ARTEMIS_URL = "tcp://localhost:61616";
    private static final String WEBLOGIC_URL = "t3://172.16.132.1:4311,172.16.132.1:4321";

    public static void main(String... args) {
        Quarkus.run(Bridge.class, args);
    }

    public static class Bridge implements QuarkusApplication {

        @Override
        public int run(String... args) throws Exception {
            int exitCode = 0;
            System.out.println("Startup...");
            //try {
                // Step 1. Create JNDI contexts for source and target servers
                //InitialContext weblogicContext = createContext(WEBLOGIC_URL);
                //InitialContext artemisContext = createContext(targetServer);

                //Hashtable<String, String> weblogicJndiParams = createJndiParams(WEBLOGIC_URL);
                //Hashtable<String, String> artemisJndiParams = createJndiParams(targetServer);

                ActiveMQServer server = ActiveMQServers.newActiveMQServer(new ConfigurationImpl()
                        .setName("Broker-Local")
                        .setPersistenceEnabled(true)
                        .setJournalDirectory("target/data/journal")
                        .setSecurityEnabled(false)
                        .addAcceptorConfiguration("invm", "vm://0"));

                server.start();

                System.out.println("ActiveMQServer running....");

                /*JMSBridge bridge = new JMSBridgeImpl(
                        new JNDIConnectionFactoryFactory(sourceJndiParams, "ConnectionFactory"),
                        new JNDIConnectionFactoryFactory(targetJndiParams, "ConnectionFactory"),
                        new JNDIDestinationFactory(sourceJndiParams, "source/topic"),
                        new JNDIDestinationFactory(targetJndiParams, "target/queue"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        5000,
                        10,
                        QualityOfServiceMode.AT_MOST_ONCE,
                        1,
                        -1,
                        null,
                        null,
                        true);

                bridge.start();*/
            //}catch (Exception e) {
             //   exitCode = 1;
            //}


            Quarkus.waitForExit();
            return exitCode;
        }
    }

    private static InitialContext createContext(final String server) throws Exception {
        Hashtable<String, String> jndiProps = createJndiParams(server);
        return new InitialContext(jndiProps);
    }

    private static Hashtable<String, String> createJndiParams(String server) {
        Hashtable<String, String> jndiProps = new Hashtable<>();
        jndiProps.put("connectionFactory.ConnectionFactory", server);
        jndiProps.put("java.naming.factory.initial", "org.apache.activemq.artemis.jndi.ActiveMQInitialContextFactory");
        jndiProps.put("queue.target/queue", "target");
        jndiProps.put("topic.source/topic", "source");
        return jndiProps;
    }
}
